variable "agent_namespace" {
  type        = string
  default     = "gitlab"
  description = "Kubernetes namespace to install the Agent"
}

variable "agent_token" {
  type        = string
  description = "Agent token (provided after registering an Agent in GitLab)"
  sensitive   = true
}

variable "kas_address" {
  type        = string
  description = "Agent Server address (provided after registering an Agent in GitLab)"
  default     = "wss://kas.gitlab.com"
}
