# 1.0.0 (2022-12-31)


### Features

* initial release ([36045bd](https://gitlab.com/beepbeepgo/public/terraform/gitlab-agent/commit/36045bdd8ef9a67cb3948003e43a82077692fe52))

# 1.0.0-issue-init.1 (2022-12-31)


### Features

* init release ([f12693d](https://gitlab.com/beepbeepgo/public/terraform/gitlab-agent/commit/f12693db088f64bcd37a9bda40d6e5297b9d8b6a))
