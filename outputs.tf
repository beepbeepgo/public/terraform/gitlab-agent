output "agent_name" {
  description = "Name of the gitlab agent"
  value       = resource.helm_release.gitlab_agent.name
}

output "agent_namespace" {
  description = "Kubernetes namespace the Agent is installed in"
  value       = resource.helm_release.gitlab_agent.namespace
}
