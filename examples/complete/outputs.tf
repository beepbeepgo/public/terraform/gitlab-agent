output "agent_namespace" {
  description = "Kubernetes namespace the Agent is installed in"
  value       = module.gitlab_agent.agent_namespace
}

output "agent_name" {
  description = "Name of the gitlab agent"
  value       = module.gitlab_agent.agent_name
}
