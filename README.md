# gitlab-agent

A terraform module to deploy the gitlab agent into an EKS cluster.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_helm"></a> [helm](#provider\_helm) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [helm_release.gitlab_agent](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_agent_namespace"></a> [agent\_namespace](#input\_agent\_namespace) | Kubernetes namespace to install the Agent | `string` | `"gitlab"` | no |
| <a name="input_agent_token"></a> [agent\_token](#input\_agent\_token) | Agent token (provided after registering an Agent in GitLab) | `string` | n/a | yes |
| <a name="input_kas_address"></a> [kas\_address](#input\_kas\_address) | Agent Server address (provided after registering an Agent in GitLab) | `string` | `"wss://kas.gitlab.com"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_agent_name"></a> [agent\_name](#output\_agent\_name) | Name of the gitlab agent |
| <a name="output_agent_namespace"></a> [agent\_namespace](#output\_agent\_namespace) | Kubernetes namespace the Agent is installed in |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
